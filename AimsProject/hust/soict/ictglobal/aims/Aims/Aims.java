import disc.DigitalVideoDisc;
import order.Order;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Order Order1 = new Order();
		Order Order2 = new Order();
		//Create new dvd object and set the feilds
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		//add the dvd to the order
		Order1.addDigitalVideoDisc(dvd1);
		
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
		dvd2.setCategory("Science Fiction");
		dvd2.setCost(24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);
		Order1.addDigitalVideoDisc(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
		dvd3.setCategory("Animation");
		dvd3.setCost(18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);
		Order1.addDigitalVideoDisc(dvd3);
	
		DigitalVideoDisc dvd4 = new DigitalVideoDisc("Fairy Tail");
		dvd4.setCategory("Animation");
		dvd4.setCost(22.5f);
		dvd4.setDirector("Mashima Hiro");
		dvd4.setLength(50);
		Order1.addDigitalVideoDisc(dvd4);
		
		DigitalVideoDisc dvd5 = new DigitalVideoDisc("Sword Art Online");
		dvd5.setCategory("Animation");
		dvd5.setCost(29.99f);
		dvd5.setDirector("Kawahara Reki");
		dvd5.setLength(53);
		Order1.addDigitalVideoDisc(dvd5);
		
		DigitalVideoDisc dvd6 = new DigitalVideoDisc("Your name");
		dvd6.setCategory("Animation");
		dvd6.setCost(25.66f);
		dvd6.setDirector("Shinkai Makoto");
		dvd6.setLength(35);
		Order1.addDigitalVideoDisc(dvd6);
		
		Order1.removeDigitalVideoDisc(dvd4);
		Order1.printListOrdered();
		DigitalVideoDisc dvdList[] = {dvd1, dvd3, dvd4}; 
		Order2.addDigitalVideoDisc(dvdList);
		Order2.printListOrdered();
	}

}
