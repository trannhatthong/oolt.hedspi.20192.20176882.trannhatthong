package order;
import java.util.Date;

import disc.DigitalVideoDisc;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	private int qtyOrdered = 0, i, j;
	private float totalCostOrdered = 0;
	private Date dateOrdered;
	private static int nbOrders = 0;
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}
	
	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(qtyOrdered < 10) {
			qtyOrdered += 1;
			itemOrdered[qtyOrdered-1] = disc;
			System.out.println("The disc \"" + disc.getTitle() + "\" has been added");
		}else {
			System.out.println("The order is almost full and the disc \"" + disc.getTitle() + "\" hasn\'t been added");
		}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for (i = 0; i < this.qtyOrdered; i++)
		{
			if (this.itemOrdered[i] == disc)
			{
				for (j = i; j < this.qtyOrdered; j++)
					this.itemOrdered[j] = this.itemOrdered[j+1];
				this.qtyOrdered -= 1;
				System.out.println("The disc \"" + disc.getTitle() + "\" has been removed");
			}
		}
	}
	
	public float totalCost() {
		DigitalVideoDisc newdisc = getALuckyItem();
		for (i = 0; i < this.qtyOrdered; i++)
		{	
			if (this.itemOrdered[i] == newdisc) {
				return this.totalCostOrdered;
			} else {
			this.totalCostOrdered += this.itemOrdered[i].getCost()*this.itemOrdered[i].getLength();
			}
		}
		return this.totalCostOrdered;
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList)
	{
		for (i = 0; i < dvdList.length; i++)
		{
			this.addDigitalVideoDisc(dvdList[i]);
		}
	}
//	
//	public void addDigitalVideoDisc(DigitalVideoDisc... disc) {
//		for (i = 0; i < disc.length; i++)
//			{
//				this.addDigitalVideoDisc(disc[i]);
//			}
//	}

	public Date getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Date dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	public void printListOrdered() {
		System.out.println("********************************Order********************************");
		System.out.println("Date: " + this.dateOrdered);
		System.out.println("Ordered Items:");
		for (i = 1; i <= this.qtyOrdered; i++)
		{
			System.out.println(i + ". DVD - " + this.itemOrdered[i-1].getTitle() + " - " + this.itemOrdered[i-1].getCategory() + " - " + this.itemOrdered[i-1].getDirector() + " - " + this.itemOrdered[i-1].getLength() + ": " + this.itemOrdered[i-1].getCost() + "$");
		}
		System.out.println("Your lucky item is: " + getALuckyItem().getTitle());
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("*********************************************************************");
	}

	public Order() {
		super();
		this.dateOrdered = new Date();
		if (Order.nbOrders < Order.MAX_LIMITTED_ORDERS)
		{
			Order.nbOrders ++;
		}
		else {
			System.out.println("MAX LIMITTED ORDERS!");
		}
	}
	
	public DigitalVideoDisc getALuckyItem() {
		return itemOrdered[(int) (Math.random() * ((qtyOrdered - 1) + 1) + 1)];
	}
	
}
