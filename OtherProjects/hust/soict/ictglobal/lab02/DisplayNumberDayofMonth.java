import java.util.Scanner;
public class DisplayNumberDayofMonth{
    public static void main(String[] args){
        Scanner Input = new Scanner(System.in);
        int day = 0, month, year;
        String strMonth = "";

        System.out.print("Please input year: ");
        year = Input.nextInt();
        while (year < 1){
            System.out.print("Please input year > 0, try again: ");
            year = Input.nextInt();
        }

        System.out.print("Please input month: ");
        month = Input.nextInt();
        while (month < 1 || month > 12)
        {
            System.out.print("Please input month(from 1 to 12), try again: ");
            month = Input.nextInt();
        }

        switch(month){
            case 1: 
            {
                day = 31;
                strMonth = "January";
            }
            break;
            case 2:
            {
                if (isLeapYear(year) == 1)
                {
                    day = 29;
                }
                else 
                {
                    day = 28;
                }
                strMonth = "February";
            }
            break;
            case 3: 
            {
                day = 31;
                strMonth = "March";
            }
            break;
            case 4: 
            {
                day = 30;
                strMonth = "April";
            }
            break;
            case 5: 
            {
                day = 31;
                strMonth = "May";
            }
            break;
            case 6: 
            {
                day = 30;
                strMonth = "June";
            }
            break;
            case 7: 
            {
                day = 31;
                strMonth = "July";
            }
            break;
            case 8: 
            {
                day = 31;
                strMonth = "August";
            }
            break;
            case 9: 
            {
                day = 30;
                strMonth = "September";
            }
            break;
            case 10: 
            {
                day = 31;
                strMonth = "October";
            }
            break;
            case 11: 
            {
                day = 30;
                strMonth = "November";
            }
            break;
            case 12: 
            {
                day = 31;
                strMonth = "December";
            }
            break;
        }
        System.out.println("Number of days of " + strMonth +", " + year + ": " + day);
    }
    
   static int isLeapYear (int year) {
	   int result = 0;
	   if (year % 4 == 0) result = 1;
	   if (year % 100 == 0) result = 0;
	   if (year % 400 == 0) result = 1;
	   return result;
   }
}