import java.util.Scanner;
public class AddtwoMatrics{
    public static void main(String[] args){
    	AddtwoMatrics ATM = new AddtwoMatrics();
        Scanner Input = new Scanner(System.in);
        int rows, columns;
        System.out.print("Enter number of rows: ");
        rows = Input.nextInt();
        System.out.print("Enter number of columns: ");
        columns = Input.nextInt();

        int matrix1[][] = new int[rows][columns];
        int matrix2[][] = new int[rows][columns];
        int sum[][] = new int[rows][columns];

        System.out.println("Enter all elements of the first matrix: ");
        matrix1 = insertMatrices(rows,columns);

        System.out.println("Enter all elements of the second matrix: ");
        matrix2 = insertMatrices(rows,columns);
        
        sum = sumM(matrix1, matrix2, rows, columns);
        System.out.println("The first matrix: ");
        printMatrices(matrix1,rows,columns);
        System.out.println("The second matrix: ");
        printMatrices(matrix2,rows,columns);
        System.out.println("The sum of two matrices: ");
        printMatrices(sum,rows,columns);
    }
	static void printMatrices (int a[][], int rows,int columns) {
    	for (int i = 0; i < rows; i++)
        {
            for(int  j = 0; j < columns; j++)
                System.out.print(a[i][j] + "\t");
            System.out.println();
        }
    }
	
	static int[][] insertMatrices (int rows, int columns){
		Scanner Input = new Scanner(System.in);
		int a[][] = new int[rows][columns];
		for (int i = 0; i < rows; i++)
            for(int j = 0; j < columns; j++)
            {
            	System.out.printf("[%d][%d]:",i + 1, j + 1);
                a[i][j] = Input.nextInt();
            }
		return a;
	}
	static int[][] sumM (int a[][], int b[][],int rows, int columns) {
		int sum[][] = new int[rows][columns];
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
          {
				sum[i][j] = a[i][j] + b[i][j];
          }
		return sum;
	}
}