import java.util.Scanner;
import java.util.Arrays;
public class SortArrayandCaculate{
    public static void main(String[] args){
        Scanner Input = new Scanner(System.in);
        float average;
        int sum = 0, i, j, n, temp;

        System.out.print("Enter number of elements you want in array: ");
        n = Input.nextInt();
        int a[] = new int[n];
        System.out.println("Enter all numbers: ");
        for (i = 0; i < n; i++)
        {
            a[i] = Input.nextInt();
            sum += a[i];
        }
        average = (float)sum/n;
        System.out.println("Origin array: " + Arrays.toString(a));
        for (i = 0; i < n; i++)
        {
            for (j = i+1; j < n; j++)
            {
                if (a[i] > a[j])
                {
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
        System.out.println("Sort array: " + Arrays.toString(a));
        System.out.println("Sum of array: " + sum);
        System.out.println("Average of array: " + average);
    }
}