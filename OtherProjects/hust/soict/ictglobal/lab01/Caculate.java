package hust.soict.ictglobal.lab01;
//Write a program to calculate sum, difference, product, and quotient of 2 double numbers which are entered by users.
import javax.swing.JOptionPane;
public class Caculate{
    public static void main(String[] args){
        String strNum1, strNum2;
        Double Num1, Num2, Sum, Difference, Product, Quotient;
        String strNotification = "You've just entered: ";

        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        Num1 = Double.parseDouble(strNum1);
        
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        while (strNum2 == "0"){
            strNum2 = JOptionPane.showInputDialog(null, "Second number must be difference 0, Please input again: ", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        }
        Num2 = Double.parseDouble(strNum2);
        strNotification += strNum1 + " and " + strNum2 + ". Result:";

        Sum = Num1 + Num2;
        strNotification += "\n Sum: " + Sum;

        Difference = Num1 - Num2;
        strNotification += "\n Differenct: " + Difference;

        Product = Num1*Num2;
        strNotification += "\n Product: " + Product;

        Quotient = Num1/Num2;
        strNotification += "\n Quotient: " + Quotient;

        JOptionPane.showMessageDialog(null, strNotification, "Show result of caculation", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}