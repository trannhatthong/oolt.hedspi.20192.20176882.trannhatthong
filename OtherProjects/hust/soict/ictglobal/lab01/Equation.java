import javax.swing.JOptionPane;
public class Equation{
    public static void main(String[] args){
        String strSelection;
        String strListEquation = "Select type of equation:\n"
        		+ " 1. The first-degree equation (linear equation) with one variable\n"
        		+ " 2. The system of first-degree equations (linear equation) with two variables\n"
        		+ " 3. The second-degree equation with one variable\n"
        		+ " 4. Quit\n Select:";
        strSelection = JOptionPane.showInputDialog(null, strListEquation, "Select type of equation", JOptionPane.INFORMATION_MESSAGE);
        while (strSelection != "4")
        {
            switch (strSelection){
                case "1":
                {
                    String strA, strB;
                    String strNotification = "A first-degree equation with one variable can have a form such as ax + b = 0 (a≠0).\n Please input a and b.\n";
                    String strResult = "The equation has a solution x = ";
                    Double a, b, x;
                    
                    JOptionPane.showMessageDialog(null, strNotification, "A first-degree equation", JOptionPane.INFORMATION_MESSAGE);
                    
                    strA = JOptionPane.showInputDialog(null, "Please input a: ", "Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                    while (strA == "0"){
                        strA = JOptionPane.showInputDialog(null, "Please input a difference 0: ", "Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                    }
                    a = Double.parseDouble(strA);

                    strB = JOptionPane.showInputDialog(null, "Please input b: ", "Input the second coefficient", JOptionPane.INFORMATION_MESSAGE);
                    b = Double.parseDouble(strB);

                    x = (-b)/a;
                    
                    strResult += x;
                    JOptionPane.showMessageDialog(null, strResult, "Solution of the equation", JOptionPane.INFORMATION_MESSAGE);
                    strSelection = JOptionPane.showInputDialog(null, strListEquation, "Select type of equation", JOptionPane.INFORMATION_MESSAGE);               
                }
                    break;
                case "2":
                {
                    String strA1, strB1, strA2, strB2, strC1, strC2;
                    String strNotification = "A system of first-degree equations with two variables have two equations: a11.x1 + a12.x2 = b and a12.x1 + a22.x2 = b2.\n Please input coefficients a11, a12, b11, b12, b1, b2.\n";
                    String strResult;
                    Double a1, b1, a2, b2, c1, c2, x1, x2;
                    Double D, D1, D2;
                    
                    JOptionPane.showMessageDialog(null, strNotification, "A system of first-degree equations", JOptionPane.INFORMATION_MESSAGE);
                    
                    strA1 = JOptionPane.showInputDialog(null, "Please input a1: ", "Input coefficients", JOptionPane.INFORMATION_MESSAGE);
                    a1 = Double.parseDouble(strA1);
                    strB1 = JOptionPane.showInputDialog(null, "Please input b2: ", "Input coefficients", JOptionPane.INFORMATION_MESSAGE);
                    b1 = Double.parseDouble(strB1);
                    strC1 = JOptionPane.showInputDialog(null, "Please input c1: ", "Input coefficients", JOptionPane.INFORMATION_MESSAGE);
                    c1 = Double.parseDouble(strC1);
                    strA2 = JOptionPane.showInputDialog(null, "Please input a2: ", "Input coefficients", JOptionPane.INFORMATION_MESSAGE);
                    a2 = Double.parseDouble(strA2);
                    strB2 = JOptionPane.showInputDialog(null, "Please input b2: ", "Input coefficients", JOptionPane.INFORMATION_MESSAGE);
                    b2 = Double.parseDouble(strB2);
                    strC2 = JOptionPane.showInputDialog(null, "Please input c2: ", "Input coefficients", JOptionPane.INFORMATION_MESSAGE);
                    c2 = Double.parseDouble(strC2);

                    D = a1 * b2 - a2 * b1;
                    D1 = c1 * b2 - c2 * b1;
                    D2 = a1 * c2 - a2 * c1;

                    if (D != 0)
                    {
                        x1 = D1/D;
                        x2 = D2/D;
                        strResult = "The system has a one solution (x1, x2) = (" + x1 + ", " + x2 + ").";    
                    } 
                    else if (D1 == 0 && D2 == 0)
                    {
                        strResult = "The system has infinite solutions.";
                    }
                    else
                    {
                        strResult = "The system has no solution.";
                    }
                    JOptionPane.showMessageDialog(null, strResult, "Solution of the system", JOptionPane.INFORMATION_MESSAGE);
                    strSelection = JOptionPane.showInputDialog(null, strListEquation, "Select type of equation", JOptionPane.INFORMATION_MESSAGE);
                }
                    break;
                case "3":
                {
                    String strA, strB, strC;
                    String strNotification = "A second-degree equation with one variable can have a form such as ax^2 + bx + c = 0 (a≠0).\n Please input a, b and c.\n";
                    String strResult;
                    Double a, b, c, delta, x1, x2;
                    
                    JOptionPane.showMessageDialog(null, strNotification, "A second-degree equation", JOptionPane.INFORMATION_MESSAGE);
                    
                    strA = JOptionPane.showInputDialog(null, "Please input a: ", "Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                    while (strA == "0"){
                        strA = JOptionPane.showInputDialog(null, "Please input a difference 0: ", "Input the first coefficient", JOptionPane.INFORMATION_MESSAGE);
                    }
                    a = Double.parseDouble(strA);

                    strB = JOptionPane.showInputDialog(null, "Please input b: ", "Input the second coefficient", JOptionPane.INFORMATION_MESSAGE);
                    b = Double.parseDouble(strB);

                    strC = JOptionPane.showInputDialog(null, "Please input c: ", "Input the third coefficient", JOptionPane.INFORMATION_MESSAGE);
                    c = Double.parseDouble(strC);

                    delta = b*b - 4*a*c;

                    if(delta == 0)
                    {
                        x1 = (-b)/(2*a);
                        strResult = "The equation has double root x = " + x1;
                    }
                    else if (delta > 0)
                    {
                        x1 = (-b + Math.sqrt(delta))/(2*a);
                        x2 = (-b - Math.sqrt(delta))/(2*a);
                        strResult = "The equation has two distinct roots x1 = " + x1 + " and x2 = " + x2;
                    }
                    else 
                    {
                        strResult = "The equation has no solution";
                    }
                    JOptionPane.showMessageDialog(null, strResult, "Solution of the equation", JOptionPane.INFORMATION_MESSAGE);
                    strSelection = JOptionPane.showInputDialog(null, strListEquation, "Select type of equation", JOptionPane.INFORMATION_MESSAGE);
                }
                    break;
                case "4":
                {
                    System.out.println("Quit!\n");
                    System.exit(0);
                }
                    break;
                default: 
                {
                    JOptionPane.showMessageDialog(null, "Input invalid!\n Input again", "Error", JOptionPane.INFORMATION_MESSAGE);
                    strSelection = JOptionPane.showInputDialog(null, strListEquation, "Select type of equation", JOptionPane.INFORMATION_MESSAGE);
                } 
            }
        }
    }
}